from django.db import models
from datetime import datetime

# Create your models here.

class Project(models.Model):
    title = models.CharField(max_length = 27)
    start = models.DateField()
    deadline = models.DateField()
    desc = models.CharField(max_length = 200)
    place = models.CharField(max_length = 50)
    category = models.CharField(max_length = 27)