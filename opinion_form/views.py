from django.shortcuts import render
from .forms import Project_Form
from .models import Project

# Create your views here.
def page1(request):
    if request.method == "POST":
        iniForm = Project_Form(request.POST)
        if iniForm.is_valid():
            iniForm.save()
    else:        
        iniForm = Project_Form()

    dariDB = Project.objects.all()
    content = {'form': iniForm, 'data': dariDB}
    return render(request, 'page1.html', content)

        
