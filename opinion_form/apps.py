from django.apps import AppConfig


class OpinionFormConfig(AppConfig):
    name = 'opinion_form'
