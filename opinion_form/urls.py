from django.urls import include, path
from .views import page1

urlpatterns = [
    path('', page1, name='page1'),
]