from django import forms
from datetime import datetime, date
from .models import Project
class Project_Form(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['title', 'start', 'deadline','desc', 'place', 'category']
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'title',}),
            'start': forms.TextInput(attrs={'placeholder': 'MM/DD/YYYY',}),
            'deadline': forms.TextInput(attrs={'placeholder': 'MM/DD/YYYY',}),
            'description': forms.Textarea(attrs={'placeholder': 'desc',}),
            'place': forms.TextInput(attrs={'placeholder': 'place',}),
            'category': forms.TextInput(attrs={'placeholder': 'category',})
        }